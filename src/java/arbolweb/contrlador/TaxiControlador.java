/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbolweb.contrlador;
import arbolse.modelo.excepciones.ArbolBinarioExcepciones;
import javax.inject.Named;
import arbolweb.controlador.util.JsfUtil;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import taxiavl.modelo.NodoAVL;
import taxiavl.modelo.Taxi;

/**
 *
 * @author juand
 */
@Named(value = "taxiControlador")
@SessionScoped
public class TaxiControlador implements Serializable {
private DefaultDiagramModel model;


private Taxi arboltaxi  =new Taxi();
private Taxi dato=new Taxi();
    /**
     * Creates a new instance of TaxiControlador
     */
    public TaxiControlador() {
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public Taxi getArboltaxi() {
        return arboltaxi;
    }

    public void setArboltaxi(Taxi arboltaxi) {
        this.arboltaxi = arboltaxi;
    }

    public Taxi getDato() {
        return dato;
    }

    public void setDato(Taxi dato) {
        this.dato = dato;
    }
 
    
    
    public void pintarArbolTaxi() {

        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbolT(arboltaxi.getRaiz(), model, null, 30, 0);

    }

    private void pintarArbolT(NodoAVL reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco);

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");
            elementHijo.setStyleClass("ui-diagram-element-busc");
            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);

            pintarArbolT(reco.getIzquierda(), model, elementHijo, x - 5, y + 5);
            pintarArbolT(reco.getDerecha(), model, elementHijo, x + 5, y + 5);
        }
    }
    
    
           public void adicionarNodo() throws ArbolBinarioExcepciones{
        try {
            arboltaxi.adicionarNodoTaxi(dato, arboltaxi.getRaiz());
            JsfUtil.addSuccessMessage("El dato ha sido adicionado");
            pintarArbolTaxi();
            dato=new Taxi();
            
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
     
           
   public void balancear(){
        arboltaxi.balancear(arboltaxi.getRaiz());
        pintarArbolTaxi();
        
    }
    
}
