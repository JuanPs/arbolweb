/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbolweb.contrlador;

import arbolse.modelo.ArbolBinario;
import arbolse.modelo.Nodo;
import arbolse.modelo.excepciones.ArbolBinarioExcepciones;
import arbolweb.controlador.util.JsfUtil;
import com.sun.org.apache.regexp.internal.recompile;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author juand
 */
@Named(value = "arbolBinaroControlador")
@SessionScoped
public class ArbolBinarioControlador implements Serializable {
 
    
     private ArbolBinario arbol = new ArbolBinario ();
     
       private String datoscsv = "18,15,13,17,8,14,-8,10,59,28,80,78,90";
    private int terminado;
      private ArbolBinario arbolTerminados = new ArbolBinario();

  private DefaultDiagramModel modelArbol2;
     private DefaultDiagramModel model;
   
    private int dato;
    private String  split ;
    private String arbolCI = " Arbol Completo";

    public String getArbolCI() {
        return arbolCI;
    }

    public void setArbolCI(String arbolCI) {
        this.arbolCI = arbolCI;
    }

    public String getDatoscsv() {
        return datoscsv;
    }

    public void setDatoscsv(String datoscsv) {
        this.datoscsv = datoscsv;
    }

    public int getTerminado() {
        return terminado;
    }

    public void setTerminado(int terminado) {
        this.terminado = terminado;
    }

    public ArbolBinario getArbolTerminados() {
        return arbolTerminados;
    }

    public void setArbolTerminados(ArbolBinario arbolTerminados) {
        this.arbolTerminados = arbolTerminados;
    }

    public DefaultDiagramModel getModelArbol2() {
        return modelArbol2;
    }

    public void setModelArbol2(DefaultDiagramModel modelArbol2) {
        this.modelArbol2 = modelArbol2;
    }
    
    

    public String getSplit() {
        return split;
    }

    public void setSplit(String split) {
        this.split = split;
    }
    private boolean verInOrden=false;
    private boolean verPosOrden=false;
    private boolean verPreOrden=false;
    private boolean verCantidadNodos=false;
    private boolean verCantidadHojas=false;
    private boolean verAltura=false;
    private boolean verMenorValor=false;
    private boolean verMayorValor=false;
    private boolean verBalance=false;
    private boolean verBorrarMayor=false;
    private boolean verBorrarMenor=false;
    private boolean verSplit=false;

    public boolean isVerSplit() {
        return verSplit;
    }

    public void setVerSplit(boolean verSplit) {
        this.verSplit = verSplit;
    }
    

    public boolean isVerBorrarMayor() {
        return verBorrarMayor;
    }

    public void setVerBorrarMayor(boolean verBorrarMayor) {
        this.verBorrarMayor = verBorrarMayor;
    }

    public boolean isVerBorrarMenor() {
        return verBorrarMenor;
    }

    public void setVerBorrarMenor(boolean verBorrarMenor) {
        this.verBorrarMenor = verBorrarMenor;
    }

   
    

    public boolean isVerCantidadNodos() {
        return verCantidadNodos;
    }

    public void setVerCantidadNodos(boolean verCantidadNodos) {
        this.verCantidadNodos = verCantidadNodos;
    }

    public boolean isVerCantidadHojas() {
        return verCantidadHojas;
    }

    public void setVerCantidadHojas(boolean verCantidadHojas) {
        this.verCantidadHojas = verCantidadHojas;
    }

    public boolean isVerAltura() {
        return verAltura;
    }

    public void setVerAltura(boolean verAltura) {
        this.verAltura = verAltura;
    }

    public boolean isVerMenorValor() {
        return verMenorValor;
    }

    public void setVerMenorValor(boolean verMenorValor) {
        this.verMenorValor = verMenorValor;
    }

    public boolean isVerMayorValor() {
        return verMayorValor;
    }

    public void setVerMayorValor(boolean verMayorValor) {
        this.verMayorValor = verMayorValor;
    }

    public boolean isVerBalance() {
        return verBalance;
    }

    public void setVerBalance(boolean verBalance) {
        this.verBalance = verBalance;
    }

    
    public boolean isVerPosOrden() {
        return verPosOrden;
    }

    public void setVerPosOrden(boolean verPosOrden) {
        this.verPosOrden = verPosOrden;
    }

    public boolean isVerPreOrden() {
        return verPreOrden;
    }

    public void setVerPreOrden(boolean verPreOrden) {
        this.verPreOrden = verPreOrden;
    }

    public boolean isVerInOrden() {
        return verInOrden;
    }

    public void setVerInOrden(boolean verInOrden) {
        this.verInOrden = verInOrden;
    }
    
    
    
    

    public int getDato() {
        return dato;
    }

    public void setDato(int dato) {
        this.dato = dato;
    }
    
    

    public ArbolBinario getArbol() {
        return arbol;
    }

    public void setArbol(ArbolBinario arbol) {
        this.arbol = arbol;
    }

    public ArbolBinarioControlador(DefaultDiagramModel model) {
        this.model = model;
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }
    
    

    /**
     * Creates a new instance of ArbolBinarioControlador
     */
    public ArbolBinarioControlador() {
    }
    
    
    public void adicionarNodo()
    {
        try {
            arbol.adicionarNodo(dato, arbol.getRaiz());
            JsfUtil.addSuccessMessage("El dato ha sido adicionado");
            dato=0;
            pintarArbol();
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
   
    public void habilitarInOrden()
    {
        try {
            arbol.isLleno();
            verInOrden=true;            
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
    
      public void habilitarPosOrden()
    {
        try {
            arbol.isLleno();
            verPosOrden=true;            
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
        public void habilitarPreOrden()
    {
        try {
            arbol.isLleno();
            verPreOrden=true;            
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
        
    }
        
         public void habilitarCantidadNodos()
    {
        try {
            arbol.isLleno();
            verCantidadNodos=true;            
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
       
          public void habilitarCantidadHojas()
    {
        try {
            arbol.isLleno();
            verCantidadHojas=true;            
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
           public void habilitarBalance()
    {
        try {
            arbol.isLleno();
            verBalance=true;            
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
            
        }
    }
        public void HaBorrarMayor()
    {
        try {
            arbol.isLleno();      
            verBorrarMayor=true;
            arbol.borrarMayor();
            JsfUtil.addSuccessMessage("Dato Borrado");
            pintarArbol();
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
        
            public void HaBorrarMenor()
    {
        try {
            arbol.isLleno();
            verBorrarMenor=true;
            arbol.borrarMenor();
             JsfUtil.addSuccessMessage("Dato Borrado");
            pintarArbol();
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
            public void habilitarAltura()
    {
        try {
            arbol.isLleno();
            verAltura=true;            
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
             public void habilitarMayorValor()
    {
        try {
            arbol.isLleno();
            verMayorValor=true;            
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
              public void habilitarMenorValor()
    {
        try {
            arbol.isLleno();
            verMenorValor=true;            
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
                        
        public void pintarArbol(){
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbol.getRaiz(), model, null, 30, 0);
    }
        
         private void pintarArbol(Nodo reco,DefaultDiagramModel model, Element padre, int x, int y) {
               
        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());
            
            elementHijo.setX(String.valueOf(x)+"em");
            elementHijo.setY(String.valueOf(y)+"em");
            
            if(padre!=null)
            {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre=new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);                
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));        
                
            }    
            
            model.addElement(elementHijo);
            
            pintarArbol(reco.getIzquierda(),model, elementHijo,x-5,y+5);
            pintarArbol(reco.getDerecha(),model,elementHijo,x+5, y+5);
            
            
        }
    }
         
       
         private void pintarArbol2(Nodo reco,DefaultDiagramModel model, Element padre, int x, int y) {
               
        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());
            
            elementHijo.setX(String.valueOf(x)+"em");
            elementHijo.setY(String.valueOf(y)+"em");
            
            if(padre!=null)
            {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre=new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);                
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));        
                
            }    
            
            model.addElement(elementHijo);
            
            pintarArbol(reco.getIzquierda(),model, elementHijo,x-5,y+5);
            pintarArbol(reco.getDerecha(),model,elementHijo,x+5, y+5);
            
            
        }
    }
         
       public  void extraccion ()
       {
          
         try {
             arbol.split(split);
         } catch (ArbolBinarioExcepciones ex) {
             Logger.getLogger(ArbolBinarioControlador.class.getName()).log(Level.SEVERE, null, ex);
         }
           
           pintarArbol();
       }

        public void extraerDatos() {
        try {
            arbol.setRaiz(null);
            arbol.llenarArbol(datoscsv);
            pintarArbol();
            datoscsv = "";
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage("Los datos ingresados no tienen el formato separado por comas");
        }
    }

    public void buscarTerminadosEn() {
        for (Element ele : model.getElements()) {
            ele.setStyleClass("ui-diagram-element");
            int numTerm = Integer.parseInt(ele.getData().toString());
            if (numTerm < 0) {
                numTerm *= -1;
            }
            if (numTerm % 10 == terminado) {
                ele.setStyleClass("ui-diagram-element-busc");
            }
        }
    }

    public void encontrarTerminadosEn() {
        try {
            arbolTerminados = new ArbolBinario();
            encontrarTerminadosEn(arbol.getRaiz());
            pintarArbolTerminados();
        } catch (ArbolBinarioExcepciones ex) {
            JsfUtil.addErrorMessage("Ocurrio un error generando el árbol de terminados" + ex);
        }
    }

    private void encontrarTerminadosEn(Nodo reco) throws ArbolBinarioExcepciones {
        if (reco != null) {
            int numTerm= reco.getDato();
            if(numTerm<0)
            {
                numTerm *=-1;
            }
            if(numTerm%10==terminado)
            {
                arbolTerminados.adicionarNodo(reco.getDato(), arbolTerminados.getRaiz());
            }
            encontrarTerminadosEn(reco.getIzquierda());
            encontrarTerminadosEn(reco.getDerecha());
        }
    }

    public void pintarArbolTerminados() {

        modelArbol2 = new DefaultDiagramModel();
        modelArbol2.setMaxConnections(-1);
        modelArbol2.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        modelArbol2.setDefaultConnector(connector);
        pintarArbolTerminados(arbolTerminados.getRaiz(), modelArbol2, null, 30, 0);

    }

    private void pintarArbolTerminados(Nodo reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");

            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);

            pintarArbolTerminados(reco.getIzquierda(), model, elementHijo, x - 5, y + 5);
            pintarArbolTerminados(reco.getDerecha(), model, elementHijo, x + 5, y + 5);
        }
    }
    public void completo()
    {
        arbolCI=arbol.completo();
        JsfUtil.addSuccessMessage(arbolCI);
    }
}

