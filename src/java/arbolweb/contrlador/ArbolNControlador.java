/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbolweb.contrlador;

import arboln.modelo.DatoCalificacion;
import arboln.modelo.ArbolN;
import arboln.modelo.Atraccion;
import arboln.modelo.NodoN;
import arboln.modelo.Calificacion;
import arboln.modelo.CalificacionaAtraccion;
import arboln.modelo.Visitante;
import arbolse.modelo.excepciones.ArbolBinarioExcepciones;
import arbolse.modelo.excepciones.ArbolNException;
import arbolweb.controlador.util.JsfUtil;
import arboln.modelo.ArbolAVL;
import arboln.modelo.NodoAVL;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.BarChartSeries;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.mindmap.DefaultMindmapNode;

import org.primefaces.model.mindmap.MindmapNode;

/**
 *
 * @author juand
 */
@Named(value = "arbolNControlador")
@SessionScoped
public class ArbolNControlador implements Serializable {

    private List<ArbolN> listadoGrupos = new ArrayList();
    private List<Calificacion> listaCalificaciones;
    private boolean verAgregarVis;
    private Atraccion editarAtraccion;
    private int vista;
    private Calificacion datoCalificacion = new Calificacion();
     private CalificacionaAtraccion atraccionSelec;
private ArrayList<ArbolN> lstArboles = new ArrayList<>();
 private DefaultDiagramModel model2;
    private int cantNodos;
    private boolean verModelpie = false;
    private boolean verReporte2 = false;
    private PieChartModel modelPie;
    private BarChartModel barModel;
    private BarChartModel barModel2;
    private String visitanteSeleccionado = "";
    private ArrayList<CalificacionaAtraccion> listaAtracciones = new ArrayList<>();
    private Calificacion calificacion = new Calificacion();
    private Calificacion editarCalificacion;
    private Atraccion atraccion = new Atraccion();
    private CalificacionaAtraccion calificacinAtraccion;
    private Visitante visitante;
    private ArrayList<Visitante> listaVisitantes;
    private Visitante visitantePadre;
    private NodoAVL vsInfo;
    private ArbolN arbolSelec = new ArbolN();
    private Visitante datosH = new Visitante();
    private ArbolN arbolN = new ArbolN();
    private boolean habilitarGrupoVis;
    private ArbolN visiSelect;
    private String comboP;
    private Date fecha;
    private TreeNode model;
 private Atraccion datosAtr = new Atraccion();
     private int contadorAnonimo = 0;
private Visitante comboX = new Visitante();
    public int getContadorAnonimo() {
        return contadorAnonimo;
    }

    public void setContadorAnonimo(int contadorAnonimo) {
        this.contadorAnonimo = contadorAnonimo;
    }
     
     
 
    public DefaultDiagramModel getModel2() {
        return model2;
    }

    public void setModel2(DefaultDiagramModel model2) {
        this.model2 = model2;
    }

    public int getCantNodos() {
        return cantNodos;
    }

    public void setCantNodos(int cantNodos) {
        this.cantNodos = cantNodos;
    }

    public TreeNode getModel() {
        return model;
    }

    public void setModel(TreeNode model) {
        this.model = model;
    }

    public boolean isVerModelpie() {
        return verModelpie;
    }

    public void setVerModelpie(boolean verModelpie) {
        this.verModelpie = verModelpie;
    }

    public boolean isVerReporte2() {
        return verReporte2;
    }

    public void setVerReporte2(boolean verReporte2) {
        this.verReporte2 = verReporte2;
    }

    public boolean isVerAgregarVis() {
        return verAgregarVis;
    }

    public void setVerAgregarVis(boolean verAgregarVis) {
        this.verAgregarVis = verAgregarVis;
    }

    public PieChartModel getModelPie() {
        return modelPie;
    }

    public void setModelPie(PieChartModel modelPie) {
        this.modelPie = modelPie;
    }

    public BarChartModel getBarModel() {
        return barModel;
    }

    public void setBarModel(BarChartModel barModel) {
        this.barModel = barModel;
    }

    public BarChartModel getBarModel2() {
        return barModel2;
    }

    public void setBarModel2(BarChartModel barModel2) {
        this.barModel2 = barModel2;
    }

    public String getVisitanteSeleccionado() {
        return visitanteSeleccionado;
    }

    public void setVisitanteSeleccionado(String visitanteSeleccionado) {
        this.visitanteSeleccionado = visitanteSeleccionado;
    }

    public Atraccion getDatosAtr() {
        return datosAtr;
    }

    public void setDatosAtr(Atraccion datosAtr) {
        this.datosAtr = datosAtr;
    }
    

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Visitante getDatosH() {
        return datosH;
    }

    public void setDatosH(Visitante datosH) {
        this.datosH = datosH;
    }

    
    public NodoAVL getVsInfo() {
        return vsInfo;
    }

    public void setVsInfo(NodoAVL vsInfo) {
        this.vsInfo = vsInfo;
    }

    
    public CalificacionaAtraccion getAtraccionSelec() {
        return atraccionSelec;
    }

    public void setAtraccionSelec(CalificacionaAtraccion atraccionSelec) {
        this.atraccionSelec = atraccionSelec;
    }

    public ArrayList<ArbolN> getLstArboles() {
        return lstArboles;
    }

    public void setLstArboles(ArrayList<ArbolN> lstArboles) {
        this.lstArboles = lstArboles;
    }

    public Calificacion getDatoCalificacion() {
        return datoCalificacion;
    }

    public void setDatoCalificacion(Calificacion datoCalificacion) {
        this.datoCalificacion = datoCalificacion;
    }

    public ArbolN getVisiSelect() {
        return visiSelect;
    }

    public void setVisiSelect(ArbolN visiSelect) {
        this.visiSelect = visiSelect;
    }

    public String getComboP() {
        return comboP;
    }

    public void setComboP(String comboP) {
        this.comboP = comboP;
    }

    public ArbolN getArbolN() {
        return arbolN;
    }

    public void setArbolN(ArbolN arbolN) {
        this.arbolN = arbolN;
    }

    public boolean isHabilitarGrupoVis() {
        return habilitarGrupoVis;
    }

    public void setHabilitarGrupoVis(boolean habilitarGrupoVis) {
        this.habilitarGrupoVis = habilitarGrupoVis;
    }

    public int getVista() {
        return vista;
    }

    public void setVista(int vista) {
        this.vista = vista;
    }
       private String nombre;
    private String identificacion;

    public Atraccion getEditarAtraccion() {
        return editarAtraccion;
    }

    public void setEditarAtraccion(Atraccion editarAtraccion) {
        this.editarAtraccion = editarAtraccion;
    }

    public Calificacion getEditarCalificacion() {
        return editarCalificacion;
    }

    public void setEditarCalificacion(Calificacion editarCalificacion) {
        this.editarCalificacion = editarCalificacion;
    }

    private MindmapNode root;

    private MindmapNode selectedNode;

    public List<Calificacion> getListaCalificaciones() {
        return listaCalificaciones;
    }

    public void setListaCalificaciones(List<Calificacion> listaCalificaciones) {
        this.listaCalificaciones = listaCalificaciones;
    }

    public ArrayList<CalificacionaAtraccion> getListaAtracciones() {
        return listaAtracciones;
    }

    public void setListaAtracciones(ArrayList<CalificacionaAtraccion> listaAtracciones) {
        this.listaAtracciones = listaAtracciones;
    }

    public ArrayList<Visitante> getListaVisitantes() {
        return listaVisitantes;
    }

    public void setListaVisitantes(ArrayList<Visitante> listaVisitantes) {
        this.listaVisitantes = listaVisitantes;
    }

    public ArbolN getArbolSelec() {
        return arbolSelec;
    }

    public void setArbolSelec(ArbolN arbolSelec) {
        this.arbolSelec = arbolSelec;
    }

    

    public Calificacion getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Calificacion calificacion) {
        this.calificacion = calificacion;
    }

    public Atraccion getAtraccion() {
        return atraccion;
    }

    public void setAtraccion(Atraccion atraccion) {
        this.atraccion = atraccion;
    }

    public CalificacionaAtraccion getCalificacinAtraccion() {
        return calificacinAtraccion;
    }

    public void setCalificacinAtraccion(CalificacionaAtraccion calificacinAtraccion) {
        this.calificacinAtraccion = calificacinAtraccion;
    }

    public MindmapNode getRoot() {
        return root;
    }

    public void setRoot(MindmapNode root) {
        this.root = root;
    }

    public MindmapNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(MindmapNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public List<ArbolN> getListadoGrupos() {
        return listadoGrupos;
    }

    public void setListadoGrupos(List<ArbolN> listadoGrupos) {
        this.listadoGrupos = listadoGrupos;
    }

    public Visitante getComboX() {
        return comboX;
    }

    public void setComboX(Visitante comboX) {
        this.comboX = comboX;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
    

    public Visitante getVisitante() {
        return visitante;
    }

    public void setVisitante(Visitante visitante) {
        this.visitante = visitante;
    }

    public Visitante getVisitantePadre() {
        return visitantePadre;
    }

    public void setVisitantePadre(Visitante visitantePadre) {
        this.visitantePadre = visitantePadre;
    }

    public ArbolAVL getArbol1() {
        return arbol1;
    }

    public void setArbol1(ArbolAVL arbol1) {
        this.arbol1 = arbol1;
    }

    public ArbolN getArbol() {
        return arbol;
    }

    public void setArbol(ArbolN arbol) {
        this.arbol = arbol;
    }

    private ArbolN arbol = new ArbolN();
    private ArbolAVL arbol1 = new ArbolAVL();

    /**
     * Creates a new instance of ArbolNControlador
     */
    public ArbolNControlador() {
    }

    @PostConstruct
    private void inicializar() {
        listaAtracciones = new ArrayList<>();
         listaAtracciones.add(new CalificacionaAtraccion(new Atraccion("1", "rueda", true), new ArbolAVL()));
            listaAtracciones.add(new CalificacionaAtraccion(new Atraccion("2", "caballitos", true), new ArbolAVL()));
        editarAtraccion = new Atraccion();

        listaCalificaciones = new ArrayList<>();
        listaCalificaciones.add(new Calificacion("Genial", "1", true, 0, true));
        

        listadoGrupos = new ArrayList<>();

        visitante = new Visitante("", "");
    }
    public void adicionar() throws ArbolNException {
        try {
            isVisitanteRegistrado(datosH.getIdentificacion(), listaVisitantes);
            if (arbolSelec.getRaiz() != null) {
                Visitante papa = arbolSelec.buscarPadre(arbolSelec.getRaiz(), comboX.getIdentificacion());
                arbolSelec.insertarHijo(arbolSelec.getRaiz(), datosH, papa);
                listaVisitantes = recorrerArbolNVisitantes();
                datosH = new Visitante(identificacion, nombre);
            } else {
                arbolSelec.insertarHijo(arbolSelec.getRaiz(), datosH, null);
                datosH = new Visitante(identificacion, nombre);
                mensajeSeleccionado(arbolSelec);
            }
            MindmapView(arbolSelec.getRaiz());
            JsfUtil.addSuccessMessage("Agregado Exitosamente");
            pintarVisitante();
        } catch (ArbolNException ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
 public void isVisitanteRegistrado(String id, ArrayList<Visitante> listado) throws ArbolNException {
        for (int i = 0; i < lstArboles.size(); i++) {
            if (lstArboles.get(i).getRaiz() != null) {
                listado = recorrerArbolNVisitantes(lstArboles.get(i).getRaiz(), listado);
                for (int j = 0; j < listado.size(); j++) {
                    if (listado.get(j).getIdentificacion().equals(id)) {
                        listaVisitantes = recorrerArbolNVisitantes();
                        throw new ArbolNException("Id Ya Fue Registrado");
                    }
                }
            }

        }
    }

   public void mensajeSeleccionado(ArbolN arbol) throws ArbolNException {
        arbolSelec = arbol;
        if (arbolSelec.getRaiz() != null) {
            MindmapView(arbolSelec.getRaiz());
            TreeNode nuevo = new DefaultTreeNode();
            listaVisitantes = recorrerArbolNVisitantes();
            pintarVisitante(arbolSelec.getRaiz(), nuevo);
        } else {
            listaVisitantes = new ArrayList<>();
            model = null;
            root = new DefaultMindmapNode();
            verAgregarVis = true;

        }
    }
   
       public void pintarVisitante() {
        model = new DefaultTreeNode();
        pintarVisitante(arbolSelec.getRaiz(), model);
    }

    public void pintarVisitante(NodoN reco, TreeNode model) {
        if (reco != null) {
            if (reco == arbolSelec.getRaiz()) {
                model = new DefaultTreeNode(new NodoN(new Visitante(reco.getDato().getIdentificacion(), reco.getDato().getNombre())), null);
                for (NodoN hijo1 : reco.getHijos()) {
                    TreeNode padre = new DefaultTreeNode(new NodoN(new Visitante(hijo1.getDato().getIdentificacion(), hijo1.getDato().getNombre())), model);
                    pintarVisitante(hijo1, padre);
                }
            } else {
                for (NodoN hijo1 : reco.getHijos()) {
                    TreeNode padre = new DefaultTreeNode(new NodoN(new Visitante(hijo1.getDato().getIdentificacion(), hijo1.getDato().getNombre())), model);
                    pintarVisitante(hijo1, padre);
                }
            }
            this.model = model;
        }
    }
  public void adicionarAtraccion() throws ArbolNException {
        try {
            agregarAtraccion(datosAtr);
            //lstCalificacionXAtraccion.add(new CalificacionXAtraccion(datosAtr, new ArbolBinarioAVL()));
            datosAtr = new Atraccion();
            JsfUtil.addSuccessMessage("Agregó atracción");
        } catch (ArbolNException ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
  
  
    public void agregarAtraccion(Atraccion datos) throws ArbolNException {
        for (int i = 0; i < listaAtracciones.size(); i++) {
            if (datos.getCodigo().equals(listaAtracciones.get(i).getAtraccionC().getCodigo())) {
                throw new ArbolNException("El Codigo De La Atraccion Ya Existe");
            }
        }
        listaAtracciones.add(new CalificacionaAtraccion(datos, new ArbolAVL()));
    }

   

    
    public void adicionarCalificacion() throws ArbolNException {
        try {
            agregarCalificacion(datoCalificacion);
            datoCalificacion = new Calificacion();
            JsfUtil.addSuccessMessage("Agrego atracción");
        } catch (ArbolNException ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
    
    public void agregarCalificacion(Calificacion datoC) throws ArbolNException {
        for (int i = 0; i < listaCalificaciones.size(); i++) {
            if (datoC.getCodigo().equals(listaCalificaciones.get(i).getCodigo())) {
                throw new ArbolNException("El Codigo De La Atraccion Ya Existe");
            }
        }
        listaCalificaciones.add(datoC);
    }

    public void crearGrupo() throws ArbolNException {
        lstArboles.add(new ArbolN("Grupo: " + lstArboles.size()));
    }


   

    public void seleccionar(ArbolN arbol) {
        habilitarGrupoVis = true;
        visiSelect = arbol;
        pintarArbol();

    }   

  private Visitante datosMostrar;

    public Visitante getDatosMostrar() {
        return datosMostrar;
    }

    public void setDatosMostrar(Visitante datosMostrar) {
        this.datosMostrar = datosMostrar;
    }

    public void onNodeDblselect(SelectEvent event) throws ArbolNException {
        this.selectedNode = (MindmapNode) event.getObject();
        datosMostrar = retornarDatosVisitantes(selectedNode.getLabel());

    }

    public void pintarArbol() {
        if (visiSelect.getRaiz() != null) {
            root = new DefaultMindmapNode(visiSelect.getRaiz().getDato().getNombre(), visiSelect.getRaiz().getDato(), "FFCC00", false);
            for (NodoN pivote : visiSelect.getRaiz().getHijos()) {
                root.addNode(new DefaultMindmapNode(pivote.getDato().getNombre(), pivote.getDato(), "6E9EBF", true));
            }
        } else {
            root = new DefaultMindmapNode();
        }
    }
      public ArrayList agregarALista() throws Exception {

        ArrayList<Calificacion> calificacion = new ArrayList<>();
        //now = new Date(System.currentTimeMillis());
        calificacion.add(new Calificacion( datoCalificacion.getDescripcion(),datoCalificacion.getCodigo(), datoCalificacion.isTipoCalificacion(),  datoCalificacion.getPeso(), datoCalificacion.isEstado(), fecha));
        //System.out.println(fecha);
        return calificacion;
    }

    
     public void adicionarEnEncuesta() throws Exception {
        try {
            Visitante visitante = retornarDatosVisitantes(datosH.getIdentificacion());
            if (visitante != null) {
                if (visitante.getIdentificacion().equals("0")) {
                    if (atraccionSelec.getEncuesta().getRaiz() != null) {
                        NodoAVL anonimo = retornarNodoPadre(atraccionSelec.getEncuesta().getRaiz(), visitante.getIdentificacion());
                        if (anonimo != null) {
                            anonimo.getDato().getLstCalificaciones().add(new Calificacion(datoCalificacion.getCodigo(), datoCalificacion.getDescripcion(), datoCalificacion.isTipoCalificacion(), datoCalificacion.getPeso(),datoCalificacion.isEstado(), fecha));
                        } else {
                            atraccionSelec.getEncuesta().adicionarNodo(new DatoCalificacion(visitante, agregarALista()), atraccionSelec.getEncuesta().getRaiz());
                        }
                    } else {
                        atraccionSelec.getEncuesta().adicionarNodo(new DatoCalificacion(visitante, agregarALista()), atraccionSelec.getEncuesta().getRaiz());
                    }
                } else {
                    atraccionSelec.getEncuesta().adicionarNodo(new DatoCalificacion(visitante, agregarALista()), atraccionSelec.getEncuesta().getRaiz());
                }
                datosH = new Visitante();
                JsfUtil.addSuccessMessage("El dato ha sido adicionado");
                pintarArbol();
                reporte2();
                //reporteFechas();
                verModelpie = false;
                verReporte2 = true;
            } else {
                JsfUtil.addErrorMessage("El Visitante NO Ha Sido Registrado");
            }

        } catch (ArbolNException ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    public String irEncuesta(CalificacionaAtraccion atra) throws ArbolNException {
        atraccionSelec = atra;
        pintarArbolE();
        verModelpie = false;
        return "irEncuesta";
    }
    
     public void pintarArbolE() {
        model2 = new DefaultDiagramModel();
        model2.setMaxConnections(-1);
        model2.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model2.setDefaultConnector(connector);
        pintarArbolE(atraccionSelec.getEncuesta().getRaiz(), model2, null, 30, 0);

    }
    
    
     private void pintarArbolE(NodoAVL reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco);
            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");
            elementHijo.setId(reco.getDato().getVisitante().getIdentificacion());

            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));
            }

            model.addElement(elementHijo);

            pintarArbolE(reco.getIzquierda(), model, elementHijo, x - 5, y + 5);
            pintarArbolE(reco.getDerecha(), model, elementHijo, x + 5, y + 5);
        }
    }

     public Visitante retornarDatosVisitantes(String id) throws ArbolNException {
        if (id.equals("0")) {
            return new Visitante("0", "Anonimo");
        } else {
            for (int i = 0; i < lstArboles.size(); i++) {
                if (lstArboles.get(i).getRaiz() != null) {
                    arbolSelec = lstArboles.get(i);
                    listaVisitantes = recorrerArbolNVisitantes();
                    for (int j = 0; j < listaVisitantes.size(); j++) {
                        if (listaVisitantes.get(j).getIdentificacion().equals(id)) {
                            return listaVisitantes.get(j);
                            //throw new ArbolNException("Id Ya Fue Registrado");
                        }
                    }
                }
            }
        }
        return null;
    }
     
     
    public ArrayList recorrerArbolNVisitantes() throws ArbolNException {
        if (arbolSelec.getRaiz() != null) {
            ArrayList<Visitante> listado = new ArrayList<>();
            recorrerArbolNVisitantes(arbolSelec.getRaiz(), listado);
            return listado;
        }
        throw new ArbolNException("EL Arbol Está Vacío");
    }
    
      public ArrayList recorrerArbolNVisitantes(NodoN pivote, ArrayList listado) {
        listado.add(pivote.getDato());
        for (NodoN hijo : pivote.getHijos()) {
            recorrerArbolNVisitantes(hijo, listado);
        }
        return listado;
    }

      public void reporte2() {
        barModel = new BarChartModel();
        createBarModel();
        ChartSeries atraccion1;
        for (int i = 0; i < listaAtracciones.size(); i++) {
            atraccion1 = new BarChartSeries();
            atraccion1.setLabel(listaAtracciones.get(i).getAtraccionC().getDescripcion());
            if (listaAtracciones.get(i).getEncuesta().getRaiz() != null) {
                for (int j = 0; j < listaCalificaciones.size(); j++) {
                    cantNodos = 0;
                    cantidad(listaAtracciones.get(i).getEncuesta().getRaiz(), listaCalificaciones.get(j).getDescripcion());
                    atraccion1.set(listaCalificaciones.get(j).getDescripcion(), cantNodos);
                }
                barModel.addSeries(atraccion1);
            }
        }
    }
      
      
    public void MindmapView(NodoN pivote) {
        root = new DefaultMindmapNode();
        root = new DefaultMindmapNode(arbolSelec.getRaiz().getDato().getIdentificacion(), arbolSelec.getRaiz().getDato().getIdentificacion(), "FFCC00", false);
        for (NodoN hijo : pivote.getHijos()) {
            MindmapNode prueba = new DefaultMindmapNode(hijo.getDato().getIdentificacion(), hijo.getDato().getIdentificacion(), "6e9ebf", true);
            root.addNode(prueba);
        }
    }

    public void onNodeSelect(SelectEvent event) throws ArbolNException {
        MindmapNode node = (MindmapNode) event.getObject();
        //populate if not already loaded
        if (node.getChildren().isEmpty()) {
            Object label = node.getLabel();

            if (label.equals(node.getData())) {
                NodoN pivote = arbolSelec.retornarDatosVisitanteNodo(node.getLabel(), arbolSelec.getRaiz());
                for (NodoN hijo : pivote.getHijos()) {
                    MindmapNode prueba = new DefaultMindmapNode(hijo.getDato().getIdentificacion(), hijo.getDato().getIdentificacion(), "6e9ebf", true);
                    node.addNode(prueba);
                }
            }
        }
    }
 private void createBarModel() {
        barModel.setTitle("REPORTE 2");
        barModel.setLegendPosition("ne");

        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("CALIFICACIONES");
        xAxis.setMax(12);

        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("NUMERO DE VISITANTES");
        yAxis.setMin(0);
    }
 
  
     
     public void reporteAtraccion() {
        //satisfechos insatisfechos muy satisfechos por atraccion (calificacion por nodo)
        if (atraccionSelec.getEncuesta().getRaiz() != null) {
            modelPie = new PieChartModel();
            for (int i = 0; i < listaCalificaciones.size(); i++) {
                cantNodos = 0;
                cantidad(atraccionSelec.getEncuesta().getRaiz(), listaCalificaciones.get(i).getDescripcion());
                modelPie.set(listaCalificaciones.get(i).getDescripcion(), cantNodos);
            }
            modelPie.setTitle("Reporte");
            modelPie.setLegendPosition("w");
            modelPie.setShowDataLabels(true);
            modelPie.setFill(false);
            verModelpie = true;
            JsfUtil.addSuccessMessage("Reporte Generado");
        } else {
            JsfUtil.addErrorMessage("NO Hay Datos");
        }
    }

    public void cantidad(NodoAVL reco, String Calificacion) {
        if (reco != null) {
            for (int i = 0; i < reco.getDato().getLstCalificaciones().size(); i++) {
                if (reco.getDato().getLstCalificaciones().get(i).getDescripcion().equals(Calificacion)) {
                    cantNodos++;
                }
            }
            cantidad(reco.getIzquierda(), Calificacion);
            cantidad(reco.getDerecha(), Calificacion);
        }
    }

    
        public void mostrarCalificacionesVisitante() throws ArbolNException {
        vsInfo = retornarNodoPadre(atraccionSelec.getEncuesta().getRaiz(), visitanteSeleccionado);
    }

    public NodoAVL retornarNodoPadre(NodoAVL reco, String padre) throws ArbolNException {
        if (reco.getDato().getVisitante().getIdentificacion().equals(padre)) {
            return reco;
        } else if (padre.compareTo(reco.getDato().getVisitante().getIdentificacion()) > 0 && reco.getDerecha() != null) {
            return retornarNodoPadre(reco.getDerecha(), padre);
        } else if (padre.compareTo(reco.getDato().getVisitante().getIdentificacion()) < 0 && reco.getIzquierda() != null) {
            return retornarNodoPadre(reco.getIzquierda(), padre);
        } else {
            return null;
        }
    }
    
   
    public void onClickRight() {
        String id = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("elementId");
        visitanteSeleccionado = id.replaceAll("frmDiagrama:diagram-", "");
        System.out.println(visitanteSeleccionado);

    }
    
//}
}

